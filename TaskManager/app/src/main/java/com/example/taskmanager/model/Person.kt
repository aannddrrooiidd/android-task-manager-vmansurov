package com.example.taskmanager.model

class Person(
    private val id: String,
    private val name: String,
    private val tasks: List<Task>
) {
    fun getName() { name }
    fun getTasks() { tasks }
}