package com.example.taskmanager.model

class Task(
    private val id: String,
    private var label: String?,
    private var description: String?
){
    fun getLabel() { label }
    fun getDescription() { description }
}
