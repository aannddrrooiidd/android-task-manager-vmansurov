package com.example.taskmanager.data

import android.annotation.SuppressLint
import android.content.ContentResolver
import android.database.Cursor
import android.provider.ContactsContract
import android.util.Log
import androidx.lifecycle.ViewModel
import com.example.taskmanager.model.Person
import com.example.taskmanager.model.Task

class ContactsViewModel: ViewModel() {
    private val contacts: MutableList<Person> = mutableListOf()

    @SuppressLint("Recycle", "Range")
    fun setContacts(contentResolver: ContentResolver) {
        val cursor: Cursor? = contentResolver.query(
            ContactsContract.Contacts.CONTENT_URI,
            null,
            null,
            null,
            null
        )

        if (cursor != null && cursor.moveToFirst()) {
            do {
                if (cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME) >= 0) {
                    val name =
                        cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                    val id =
                        cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
                    val tasks = mutableListOf<Task>()
                    tasks.add(Task(id = id + 0, label = "Новая заметка", description = ""))
                    val person = Person(
                        id = id,
                        name = name,
                        tasks = tasks
                    )
                    contacts.add(person)
                    Log.d("getContacts", "$id: $name")
                }
            } while (cursor.moveToNext())
            cursor.close()
        }
    }

    fun getContacts() { contacts }
}