package com.example.taskmanager

import android.annotation.SuppressLint
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.spring
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material.icons.Icons
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.minimumInteractiveComponentSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.ViewModelProvider
import com.example.taskmanager.data.ContactsViewModel
import com.example.taskmanager.model.Person
import com.example.taskmanager.ui.theme.TaskManagerTheme

class MainActivity : ComponentActivity() {

    private lateinit var contactsViewModel: ContactsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        contactsViewModel = ViewModelProvider(this).get(ContactsViewModel::class.java)
        contactsViewModel.setContacts(contentResolver)
        setContent {
            TaskManagerTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    TaskManagerApp()
                }
            }
        }
    }

}

@Composable
fun TaskManagerApp() {

    val data = mutableListOf<String>(
        "Мансуров Владимир Александрович",
        "phone 2w3ww2wwew33333wы",
        "phone 3"
    )

    TaskList(data)
}

@Composable
fun TaskList(dataList: List<String>, modifier: Modifier = Modifier) {
    LazyColumn(modifier = modifier) {
        items(dataList) {
            TaskCard(it)
        }
    }
}

@Composable
fun TaskCard(data: String) {
    var expanded by remember { mutableStateOf<Boolean>(false) }
    Card(
        modifier = Modifier
            .padding(4.dp)
            .fillMaxWidth()
    ) {

        Column (
            modifier = Modifier
                .padding(8.dp)
                .animateContentSize(
                    animationSpec = spring(
                        dampingRatio = Spring.DampingRatioNoBouncy,
                        stiffness = Spring.StiffnessMedium
                    )
                )
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .wrapContentHeight()
            ) {
                Text(
                    text = data,
                    fontSize = 20.sp,
                    modifier = Modifier
                        .weight(1f)
                )
                TaskItemButton(
                    expanded = expanded,
                    onClick = { expanded = !expanded },
                    modifier = Modifier
                )
            }
            if (expanded) {
                Text(
                    text = "eeeee",
                    fontSize = 16.sp,
                )
            }
        }
    }
}
@Composable
private fun TaskItemButton(
    expanded: Boolean,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    val rotationState = remember { mutableFloatStateOf(0f) }
    val rotation by animateFloatAsState(
        targetValue = if (expanded) 180f else 0f,
        animationSpec = tween(durationMillis = 300), label = ""
    )

    IconButton(
        onClick = {
            onClick()
            rotationState.floatValue += 180f
        },
        modifier = modifier

    ){
        Icon(
            painter = painterResource(R.drawable.arrow_down),
            contentDescription = stringResource(R.string.expand_button_content_description),
            modifier = Modifier.rotate(rotation)
        )
    }
}
@Preview(showSystemUi = true)
@Composable
fun preview() {
    TaskManagerTheme {
        // A surface container using the 'background' color from the theme
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            TaskManagerApp()
        }
    }
}